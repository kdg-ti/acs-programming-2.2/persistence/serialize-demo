import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.List;
import model.Student;

public class SerializeDemo {
  public static final String SERIAL_FILE = "data/students.ser";

  public static void main(String[] args) {
    List<Student> studentList = List.of(
        new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerp"),
        new Student(666, "Donald Muylle", LocalDate.of(1952, 6, 14), "Roeselare"),
        new Student(123, "Samuel Johnson", LocalDate.of(1709, 4, 10), "Antwerp"),
        new Student(333, "Anoushka Shankar", LocalDate.of(2003, 5, 15), "Ko Chang")
    );

    System.out.println("Before serialization:");
    studentList.forEach(System.out::println);

    try (ObjectOutputStream oos = new ObjectOutputStream(
        new FileOutputStream(SERIAL_FILE))) {
      oos.writeObject(studentList);
    } catch (IOException e) {
      e.printStackTrace();
    }

    //studentList.clear();
    List<Student> newStudents ;
    System.out.println(
        "\nAfter serialization / deserialization: (some data are null because they are marked transient)");
    try (ObjectInputStream ois = new ObjectInputStream(
        new FileInputStream(SERIAL_FILE))) {
      Object object = ois.readObject();
      newStudents = (List<Student>) object;
      newStudents.forEach(System.out::println);
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}
