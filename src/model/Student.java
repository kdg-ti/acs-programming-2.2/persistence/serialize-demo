package model;

import java.io.Serializable;
import java.time.LocalDate;

public class Student implements Serializable
{
    private final int studentIdentity;
    private final String name;
    private final LocalDate birthDate;
    private transient String residence;

    public Student(int studentIdentity, String name, LocalDate birthDate, String residence) {
        this.studentIdentity = studentIdentity;
        this.name = name;
        this.birthDate = birthDate;
        this.residence = residence;
    }

    public Student(int studentIdentity, String name, LocalDate birthDate) {
        this(studentIdentity, name, birthDate, "");
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studentIdentity, name, birthDate, residence);
    }
}
